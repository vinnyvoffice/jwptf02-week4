# Práticas

* O projeto de referência deve ser clonado do  endereço `https://gitlab.com/vinnyvoffice/jwptf02-week4.git`
* O projeto das práticas deve ser criado localmente
* O projeto das práticas deve configurado para usar o seu repositório remoto no gitlab ou github
* O projeto das práticas deve ser configurado para usar o heroku como ambiente de entrega
* A codificação deve ser feita usando Eclipse JEE ou outra IDE java com suporte a Tomcat 9, Java 8 e Maven 3
* A entrega de cada prática deve ser feita na respectiva atividade do Google Classroom informando a url da tag no repositório remoto git mais o endereço da aplicação no heroku


## prática 13 (41)

* modificando tabelas de uma base de dados relacional com spring e jdbc

### Itens

*  4101 criando um filme
*  4102 consultando todos os filmes
*  4103 consultando um filme a partir do id
*  4104 removendo um filme
*  4105 alterando um filme
*  4106 alterando o orçamento de um filme
*  4107 desfazendo a alteração de um filme com transações

## prática 14 (42)

* consultando tabelas de uma base de dados relacional com spring e jdbc

### Itens

*  4201 consultando um filme a partir do título e da data de lançamento
*  4202 consultando os filmes de um mesmo ano de lançamento
*  4203 consultando todos os filmes classificados ascendentemente por título
*  4204 contando os filmes
*  4205 consultando todos os filmes de uma determinada página com um determinado tamanho
*  4206 consultando os filmes agrupados por ano
*  4207 consultando os personagens de um filme

## prática 15 (43)

* modificando tabelas de uma base de dados relacional com spring e jpa

### Itens

*  4301 criando um filme
*  4302 consultando todos os filmes
*  4303 consultando um filme a partir do id
*  4304 removendo um filme
*  4305 alterando um filme
*  4306 alterando o orçamento de um filme
*  4307 desfazendo a alteração de um filme com transações

## prática 16 (44)

* consultando tabelas de uma base de dados relacional com spring e jpa

### Itens

*  4401 consultando um filme a partir do título e da data de lançamento
*  4402 consultando os filmes de um mesmo ano de lançamento
*  4403 consultando todos os filmes classificados ascendentemente por título
*  4404 contando os filmes
*  4405 consultando todos os filmes de uma determinada página com um determinado tamanho
*  4406 consultando os filmes agrupados por ano
*  4407 consultando os personagens de um filme

