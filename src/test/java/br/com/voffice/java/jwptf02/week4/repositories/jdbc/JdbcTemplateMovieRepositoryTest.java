package br.com.voffice.java.jwptf02.week4.repositories.jdbc;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@JdbcTest(includeFilters = @Filter(Repository.class))
@TestPropertySource(properties = { "spring.datasource.schema=classpath:create-movies.sql",
"spring.datasource.data=classpath:insert-movies.sql" })public class JdbcTemplateMovieRepositoryTest {

	@Autowired
	JdbcMovieRepository movieRepository;

	@Test
	public void findAll() {
		Assertions.assertThat(movieRepository.findAll()).hasSize(2);
	}

}
