package br.com.voffice.java.jwptf02.week4.repositories.jpa;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
public class DataJpaMovieCharacterRepositoryTest  {

	@Autowired
	DataJpaMovieCharacterRepository cut;

	@Test
	public void findAll() {
		Assertions.assertThat(cut.findAll()).hasSize(0);
	}

}
