create table movies
(
	id integer not null auto_increment,
	title varchar(200),
	released_date date,
	budget double,
	poster varchar(200),
	primary key(id)
);