package br.com.voffice.java.jwptf02.week4.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MoviesByYear {
	private Integer year;
	private Long counter;
}
