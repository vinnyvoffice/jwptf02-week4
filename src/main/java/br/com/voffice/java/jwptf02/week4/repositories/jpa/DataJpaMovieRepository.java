package br.com.voffice.java.jwptf02.week4.repositories.jpa;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.voffice.java.jwptf02.week4.entities.Movie;

@Repository
public interface DataJpaMovieRepository extends JpaRepository<Movie, Long> {

	@Query("select m from Movie m where year(m.releasedDate) = :year")
	public List<Movie> findAllByYear(@Param("year") int year);

	@Query("select m from Movie m where title = :title and year(m.releasedDate) = :year")
	public List<Movie> findByTitleAndYear(@Param("title") String title, @Param("year") int year);

	@Query("select new br.com.voffice.java.jwptf02.week4.entities.MoviesByYear(year(m.releasedDate), count(m)) from Movie m group by year(m.releasedDate)")
	public List<Map<Integer, Long>> findAllGroupByYear();

	@Modifying(clearAutomatically = true)
	@Query("update Movie m set budget = :budget where id = :id")
	public int updateBudget(@Param("budget") double budget, @Param("id") long id);


}