package br.com.voffice.java.jwptf02.week4.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.voffice.java.jwptf02.week4.entities.Actor;

@Repository
public interface DataJpaActorRepository extends JpaRepository<Actor, Long> {}