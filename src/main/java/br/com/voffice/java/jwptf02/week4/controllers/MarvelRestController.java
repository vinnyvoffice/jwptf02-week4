package br.com.voffice.java.jwptf02.week4.controllers;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.voffice.java.jwptf02.week4.entities.Actor;
import br.com.voffice.java.jwptf02.week4.entities.Movie;
import br.com.voffice.java.jwptf02.week4.entities.MovieCharacter;
import br.com.voffice.java.jwptf02.week4.repositories.jpa.DataJpaActorRepository;
import br.com.voffice.java.jwptf02.week4.repositories.jpa.DataJpaMovieCharacterRepository;
import br.com.voffice.java.jwptf02.week4.repositories.jpa.DataJpaMovieRepository;
import br.com.voffice.java.jwptf02.week4.services.MovieService;

@RestController
@RequestMapping("/api/marvel")
public class MarvelRestController {

	@Autowired
	private DataJpaMovieRepository movieRepository;

	@Autowired
	private DataJpaMovieCharacterRepository characterRepository;

	@Autowired
	private DataJpaActorRepository actorRepository;

	@Autowired
	private MovieService movieService;

	@GetMapping("movies")
	public List<Movie> getMovies(){
		return movieRepository.findAll();
	}

	@GetMapping("actors")
	public List<Actor> getActors(){
		return actorRepository.findAll();
	}

	@GetMapping("characters")
	public List<MovieCharacter> getCharacters(){
		return characterRepository.findAll();
	}

	@ResponseStatus(HttpStatus.CREATED)
	@GetMapping("iw")
	public boolean postInfinityWar() throws Exception{
		Movie iw =  Movie.builder().title("Avengers: Infinity War").releasedDate(LocalDate.of(2018, 4, 27)).poster("https://m.media-amazon.com/images/M/MV5BMjMxNjY2MDU1OV5BMl5BanBnXkFtZTgwNzY1MTUwNTM@._V1_SY1000_CR0,0,674,1000_AL_.jpg").rating(86).budget(321000000.0).characters(new LinkedList<>()).build();
		Actor downeyjr = Actor.builder().name("Robert Downey Jr").build();
		Actor hems = Actor.builder().name("Chris Hemsworth").build();
		Actor evans = Actor.builder().name("Chris Evans").build();
		Actor scarlett = Actor.builder().name("Scarlett Johansonn").build();
		Actor zoe = Actor.builder().name("Zoe Saldana").build();
		Actor olsen = Actor.builder().name("Elisabeth Olsen").build();
		MovieCharacter stark = MovieCharacter.builder().movie(iw).actor(downeyjr).name("Tony Stark / Iron Man").build();
		MovieCharacter rogers = MovieCharacter.builder().movie(iw).actor(evans).name("Steve Rogers / Captain America").build();
		MovieCharacter thor = MovieCharacter.builder().movie(iw).actor(hems).name("Thor").build();
		MovieCharacter natasha = MovieCharacter.builder().movie(iw).actor(scarlett).name("Black Window").build();
		MovieCharacter gamorra = MovieCharacter.builder().movie(iw).actor(zoe).name("Gamorra").build();
		MovieCharacter wanda = MovieCharacter.builder().movie(iw).actor(olsen).name("Wanda").build();
		iw.addCharacter(stark);
		iw.addCharacter(rogers);
		iw.addCharacter(thor);
		iw.addCharacter(natasha);
		iw.addCharacter(gamorra);
		iw.addCharacter(wanda);
		movieService.save(iw);
		return true;
	}

	@ResponseStatus(HttpStatus.CREATED)
	@GetMapping("bp")
	public boolean postBlackPanther() throws Exception{
		Movie iw =  Movie.builder().title("Black Panther").releasedDate(LocalDate.of(2018, 2, 16)).poster("https://m.media-amazon.com/images/M/MV5BMTg1MTY2MjYzNV5BMl5BanBnXkFtZTgwMTc4NTMwNDI@._V1_SY1000_CR0,0,674,1000_AL_.jpg").rating(74).budget(200000000.0).characters(new LinkedList<>()).build();
		Actor boseman = Actor.builder().name("Chadwick Boseman").build();
		Actor jordan = Actor.builder().name("Michael B. Jordan").build();
		Actor letitia = Actor.builder().name("Letitia Wright").build();
		MovieCharacter panther = MovieCharacter.builder().movie(iw).actor(boseman).name("TChalla / Black Panther").build();
		MovieCharacter erik = MovieCharacter.builder().movie(iw).actor(jordan).name("Erik Killmonger").build();
		MovieCharacter shuri = MovieCharacter.builder().movie(iw).actor(letitia).name("Shuri").build();
		iw.addCharacter(panther);
		iw.addCharacter(erik);
		iw.addCharacter(shuri);
		movieService.save(iw);
		return true;
	}
}
