package br.com.voffice.java.jwptf02.week4.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.voffice.java.jwptf02.week4.entities.Movie;
import br.com.voffice.java.jwptf02.week4.repositories.jpa.DataJpaMovieRepository;

@Service
public class MovieService {

	@Autowired
	private DataJpaMovieRepository movieRepository;

	@Transactional
	public void save(Movie movie) throws Exception {
		Movie sameKey = Movie.builder().title(movie.getTitle()).releasedDate(movie.getReleasedDate()).build();
		if (movieRepository.exists(Example.of(sameKey))) {
			throw new Exception("Filme já existe");
		}
		movieRepository.save(movie);
	}
}
