package br.com.voffice.java.jwptf02.week4.repositories.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.voffice.java.jwptf02.week4.entities.Movie;

@Repository
public class JdbcTemplateMovieRepository implements JdbcMovieRepository {

	private final Logger log = Logger.getLogger(JdbcMovieRepository.class.getName());
	private final JdbcTemplate jdbcTemplate;

	JdbcTemplateMovieRepository(JdbcTemplate jdbcTemplate){
		this.jdbcTemplate = jdbcTemplate;
	}

	public List<Movie> findAll(){
		return jdbcTemplate.query("select * from movies", new BeanPropertyRowMapper<Movie>(Movie.class));
	}

	@Override
	public List<Movie> findAllByYear(long year) {
		return jdbcTemplate.query("select * from movies m where year(m.released_date) = ?", new BeanPropertyRowMapper<Movie>(Movie.class), year);
	}

	public long count() {
		return jdbcTemplate.queryForObject("select count(id) from movies", Long.class);
	}

	@Override
	public List<Movie> findAll(Pageable pageable) {
		String sql = "select * from movies m";
		int number = pageable.getPageNumber();
		int size = pageable.getPageSize() ;
		long offset = ((number-1)*size);
		long count = count();
		offset = count < size? 0: offset;
		long limit = count < size? count: size;
		if (Objects.nonNull(pageable.getSort())) {
			for(Order o : pageable.getSort()){
				sql += " order by " + o.getProperty() + " " + o.getDirection().toString() + " ";
			}
		}
		sql += " offset " + offset + " limit " + limit + " ";

		System.out.println(sql);
		return jdbcTemplate.query(sql, new BeanPropertyRowMapper<Movie>(Movie.class));
	}

	public Optional<Movie> findByTitleAndReleasedDate(String title, LocalDate releasedDate) {
		Movie result = null;
		try {
			result = jdbcTemplate.queryForObject("select * from movies where title = ? and released_date = ?", new Object[] {title, releasedDate},  new BeanPropertyRowMapper<Movie>(Movie.class));
		} catch (EmptyResultDataAccessException e) {
			log.severe(e.getLocalizedMessage());
		}
		return Optional.ofNullable(result);
	}

	@Override
	public Movie findById(long id) throws MovieNotFoundException {
		return jdbcTemplate.queryForObject("select * from movies m where id = ?", new BeanPropertyRowMapper<Movie>(Movie.class), id);
	}



	@Transactional
	@Override
	public long create(Movie movie) throws MovieConflictException {
		final String sql = "insert into movies(title, released_date, budget, poster) values (?,?,?,?)";
		//int updatedRows = jdbcTemplate.update(sql, movie.getTitle(), movie.getReleasedDate(), movie.getBudget(), movie.getPoster());
    	KeyHolder keyHolder = new GeneratedKeyHolder();
    	Optional<Movie> optional = this.findByTitleAndReleasedDate(movie.getTitle(), movie.getReleasedDate());

    	jdbcTemplate.update(
    	    new PreparedStatementCreator() {
    	        public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
    	            PreparedStatement pst =
    	                con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
    	            pst.setString(1, movie.getTitle());
    	            pst.setObject(2, movie.getReleasedDate());
    	            pst.setObject(3, movie.getBudget());
    	            pst.setString(4, movie.getPoster());
    	            return pst;
    	        }
    	    },
    	    keyHolder);
    	movie.setId(keyHolder.getKey().longValue());
    	if (optional.isPresent()) {
    		//no rollback on checked exception
			throw new MovieConflictException(movie);
    	}
    	if ("ghost 2".equals(movie.getTitle())){
    		//rollback on unchecked exception
			throw new RuntimeException("Filme ghost 2 é proibido");
		}
		return movie.getId();
	}

	@Override
	public boolean update(Movie movie) throws MovieNotFoundException {
		int updatedRows = jdbcTemplate.update("update movies set title = ? where id = ?", movie.getTitle(), movie.getId());
		return updatedRows > 0;
	}

	@Override
	public boolean remove(Movie movie) throws MovieNotFoundException {
		 int removedRows = jdbcTemplate.update("delete from movies where id = ?", movie.getId());
		 return removedRows > 0;
	}

	public void clear() {
		jdbcTemplate.update("delete from movies");
	}

}
