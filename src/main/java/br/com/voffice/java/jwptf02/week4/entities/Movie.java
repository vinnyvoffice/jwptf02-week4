package br.com.voffice.java.jwptf02.week4.entities;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name="movies")
@Data
@Builder
@JsonInclude(Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude="characters")
public  class Movie {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String title;
	@Column(name = "released_date")
	private LocalDate releasedDate;
	private Double budget;
	private String poster;
	private Integer rating;
	private String category;
	private Boolean result;
	@OneToMany(cascade=CascadeType.ALL, mappedBy="movie")
	private List<MovieCharacter> characters;

	public void addCharacter(MovieCharacter character) {
		this.getCharacters().add(character);
	}

}
