package br.com.voffice.java.jwptf02.week4.repositories.jdbc;

import java.util.List;

import org.springframework.data.domain.Pageable;

import br.com.voffice.java.jwptf02.week4.entities.Movie;

public interface JdbcMovieRepository {

	List<Movie> findAll();
	List<Movie>  findAllByYear(long year);
	List<Movie>  findAll(Pageable pageable);
	Movie findById(long id)  throws MovieNotFoundException;
	long create(Movie movie) throws MovieConflictException;
	boolean update(Movie movie) throws MovieNotFoundException;
	boolean remove(Movie movie) throws MovieNotFoundException;

	public static class MovieNotFoundException extends Exception {

		private static final long serialVersionUID = 1L;

	}

	public static class MovieConflictException extends Exception {

		private static final long serialVersionUID = 1L;


		public MovieConflictException(Movie movie) {
			super(String.format("Movie %s(%s) already exists", movie.getTitle(), movie.getReleasedDate().getYear()));
		}
	}
}
